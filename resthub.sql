-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- ホスト: 127.0.0.1
-- 生成日時: 
-- サーバのバージョン： 10.4.6-MariaDB
-- PHP のバージョン: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `resthub`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `admin_menu`
--

CREATE TABLE `admin_menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `order` int(11) NOT NULL DEFAULT 0,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- テーブルのデータのダンプ `admin_menu`
--

INSERT INTO `admin_menu` (`id`, `parent_id`, `order`, `title`, `icon`, `uri`, `permission`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 'Dashboard', 'fa-bar-chart', '/', NULL, NULL, NULL),
(2, 0, 2, 'Admin', 'fa-tasks', '', NULL, NULL, NULL),
(3, 2, 3, 'Users', 'fa-users', 'auth/users', NULL, NULL, NULL),
(4, 2, 4, 'Roles', 'fa-user', 'auth/roles', NULL, NULL, NULL),
(5, 2, 5, 'Permission', 'fa-ban', 'auth/permissions', NULL, NULL, NULL),
(6, 2, 6, 'Menu', 'fa-bars', 'auth/menu', NULL, NULL, NULL),
(7, 2, 7, 'Operation log', 'fa-history', 'auth/logs', NULL, NULL, NULL),
(8, 0, 0, 'Git', 'fa-git', NULL, NULL, '2019-10-20 17:50:28', '2019-10-20 17:50:28'),
(9, 8, 0, 'GitType', 'fa-bars', 'git-types', NULL, '2019-10-20 17:51:36', '2019-10-20 17:51:36'),
(10, 8, 0, 'GitServer', 'fa-bars', 'git-servers', NULL, '2019-10-20 17:52:06', '2019-10-20 17:52:06');

-- --------------------------------------------------------

--
-- テーブルの構造 `admin_operation_log`
--

CREATE TABLE `admin_operation_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `input` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- テーブルのデータのダンプ `admin_operation_log`
--

INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'GET', '127.0.0.1', '[]', '2019-10-17 22:41:04', '2019-10-17 22:41:04'),
(2, 1, 'admin/auth/setting', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-17 22:41:35', '2019-10-17 22:41:35'),
(3, 1, 'admin/auth/setting', 'GET', '127.0.0.1', '[]', '2019-10-17 22:42:52', '2019-10-17 22:42:52'),
(4, 1, 'admin/auth/setting', 'GET', '127.0.0.1', '[]', '2019-10-17 22:43:00', '2019-10-17 22:43:00'),
(5, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-17 22:43:13', '2019-10-17 22:43:13'),
(6, 1, 'admin', 'GET', '127.0.0.1', '[]', '2019-10-17 23:18:17', '2019-10-17 23:18:17'),
(7, 1, 'admin', 'GET', '127.0.0.1', '[]', '2019-10-17 23:19:56', '2019-10-17 23:19:56'),
(8, 1, 'admin/git-servers', 'GET', '127.0.0.1', '[]', '2019-10-17 23:20:10', '2019-10-17 23:20:10'),
(9, 1, 'admin/git-servers', 'GET', '127.0.0.1', '[]', '2019-10-17 23:58:40', '2019-10-17 23:58:40'),
(10, 1, 'admin/git-servers', 'GET', '127.0.0.1', '[]', '2019-10-18 00:00:25', '2019-10-18 00:00:25'),
(11, 1, 'admin/git-servers/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-18 00:00:30', '2019-10-18 00:00:30'),
(12, 1, 'admin/git-servers/create', 'GET', '127.0.0.1', '[]', '2019-10-20 16:51:22', '2019-10-20 16:51:22'),
(13, 1, 'admin/git-types', 'GET', '127.0.0.1', '[]', '2019-10-20 16:51:43', '2019-10-20 16:51:43'),
(14, 1, 'admin/git-types/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 16:51:54', '2019-10-20 16:51:54'),
(15, 1, 'admin/git-types/create', 'GET', '127.0.0.1', '[]', '2019-10-20 16:59:22', '2019-10-20 16:59:22'),
(16, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 16:59:38', '2019-10-20 16:59:38'),
(17, 1, 'admin/git-types', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 16:59:40', '2019-10-20 16:59:40'),
(18, 1, 'admin/git-types/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 16:59:50', '2019-10-20 16:59:50'),
(19, 1, 'admin/git-types/create', 'GET', '127.0.0.1', '[]', '2019-10-20 17:00:38', '2019-10-20 17:00:38'),
(20, 1, 'admin/git-types/create', 'GET', '127.0.0.1', '[]', '2019-10-20 17:00:47', '2019-10-20 17:00:47'),
(21, 1, 'admin/git-types/create', 'GET', '127.0.0.1', '[]', '2019-10-20 17:01:42', '2019-10-20 17:01:42'),
(22, 1, 'admin/git-types/create', 'GET', '127.0.0.1', '[]', '2019-10-20 17:02:13', '2019-10-20 17:02:13'),
(23, 1, 'admin/git-types/create', 'GET', '127.0.0.1', '[]', '2019-10-20 17:19:55', '2019-10-20 17:19:55'),
(24, 1, 'admin/git-types/create', 'GET', '127.0.0.1', '[]', '2019-10-20 17:20:50', '2019-10-20 17:20:50'),
(25, 1, 'admin/git-types', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 17:21:02', '2019-10-20 17:21:02'),
(26, 1, 'admin/git-types/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 17:21:07', '2019-10-20 17:21:07'),
(27, 1, 'admin/git-types/create', 'GET', '127.0.0.1', '[]', '2019-10-20 17:22:09', '2019-10-20 17:22:09'),
(28, 1, 'admin/git-types/create', 'GET', '127.0.0.1', '[]', '2019-10-20 17:24:08', '2019-10-20 17:24:08'),
(29, 1, 'admin/git-types', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 17:24:13', '2019-10-20 17:24:13'),
(30, 1, 'admin/git-types/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 17:24:21', '2019-10-20 17:24:21'),
(31, 1, 'admin/git-types/create', 'GET', '127.0.0.1', '[]', '2019-10-20 17:44:51', '2019-10-20 17:44:51'),
(32, 1, 'admin/git-types', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 17:44:55', '2019-10-20 17:44:55'),
(33, 1, 'admin/git-types', 'GET', '127.0.0.1', '[]', '2019-10-20 17:45:08', '2019-10-20 17:45:08'),
(34, 1, 'admin/git-types', 'GET', '127.0.0.1', '[]', '2019-10-20 17:49:18', '2019-10-20 17:49:18'),
(35, 1, 'admin/git-types', 'GET', '127.0.0.1', '[]', '2019-10-20 17:49:22', '2019-10-20 17:49:22'),
(36, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 17:49:33', '2019-10-20 17:49:33'),
(37, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"Git\",\"icon\":\"fa-git\",\"uri\":null,\"roles\":[null],\"permission\":null,\"_token\":\"a5DhdPsJfZa3UPvsyS7IFHWPYN2SmssJritP0yDo\"}', '2019-10-20 17:50:28', '2019-10-20 17:50:28'),
(38, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2019-10-20 17:50:29', '2019-10-20 17:50:29'),
(39, 1, 'admin/auth/menu/3/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 17:50:38', '2019-10-20 17:50:38'),
(40, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 17:50:42', '2019-10-20 17:50:42'),
(41, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"8\",\"title\":\"GitType\",\"icon\":\"fa-bars\",\"uri\":\"git-types\",\"roles\":[null],\"permission\":null,\"_token\":\"a5DhdPsJfZa3UPvsyS7IFHWPYN2SmssJritP0yDo\"}', '2019-10-20 17:51:36', '2019-10-20 17:51:36'),
(42, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2019-10-20 17:51:37', '2019-10-20 17:51:37'),
(43, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2019-10-20 17:51:43', '2019-10-20 17:51:43'),
(44, 1, 'admin/git-types', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 17:51:48', '2019-10-20 17:51:48'),
(45, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 17:51:52', '2019-10-20 17:51:52'),
(46, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"8\",\"title\":\"GitServer\",\"icon\":\"fa-bars\",\"uri\":\"git-servers\",\"roles\":[null],\"permission\":null,\"_token\":\"a5DhdPsJfZa3UPvsyS7IFHWPYN2SmssJritP0yDo\"}', '2019-10-20 17:52:05', '2019-10-20 17:52:05'),
(47, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2019-10-20 17:52:06', '2019-10-20 17:52:06'),
(48, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2019-10-20 17:52:08', '2019-10-20 17:52:08'),
(49, 1, 'admin/git-servers', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 17:52:12', '2019-10-20 17:52:12'),
(50, 1, 'admin/git-types', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 17:52:15', '2019-10-20 17:52:15'),
(51, 1, 'admin/git-types', 'GET', '127.0.0.1', '[]', '2019-10-20 18:04:05', '2019-10-20 18:04:05'),
(52, 1, 'admin/git-types', 'GET', '127.0.0.1', '[]', '2019-10-20 19:44:02', '2019-10-20 19:44:02'),
(53, 1, 'admin/git-types', 'GET', '127.0.0.1', '[]', '2019-10-20 19:44:10', '2019-10-20 19:44:10'),
(54, 1, 'admin/git-types', 'GET', '127.0.0.1', '[]', '2019-10-20 19:44:53', '2019-10-20 19:44:53'),
(55, 1, 'admin/git-types/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 19:44:59', '2019-10-20 19:44:59'),
(56, 1, 'admin/git-servers', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 19:54:16', '2019-10-20 19:54:16'),
(57, 1, 'admin/git-servers/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 19:54:19', '2019-10-20 19:54:19'),
(58, 1, 'admin/git-types', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 19:55:33', '2019-10-20 19:55:33'),
(59, 1, 'admin/git-types/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 19:55:39', '2019-10-20 19:55:39'),
(60, 1, 'admin/git-servers', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 20:17:30', '2019-10-20 20:17:30'),
(61, 1, 'admin/git-types', 'GET', '127.0.0.1', '[]', '2019-10-20 20:56:54', '2019-10-20 20:56:54'),
(62, 1, 'admin/git-servers', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 20:57:02', '2019-10-20 20:57:02'),
(63, 1, 'admin/git-servers/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 20:57:04', '2019-10-20 20:57:04'),
(64, 1, 'admin/git-servers/create', 'GET', '127.0.0.1', '[]', '2019-10-20 21:26:32', '2019-10-20 21:26:32'),
(65, 1, 'admin/git-servers', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 21:26:41', '2019-10-20 21:26:41'),
(66, 1, 'admin/git-servers/create', 'GET', '127.0.0.1', '[]', '2019-10-20 21:26:47', '2019-10-20 21:26:47'),
(67, 1, 'admin/git-servers/create', 'GET', '127.0.0.1', '[]', '2019-10-20 21:27:06', '2019-10-20 21:27:06'),
(68, 1, 'admin/git-servers', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 21:27:27', '2019-10-20 21:27:27'),
(69, 1, 'admin/git-servers/create', 'GET', '127.0.0.1', '[]', '2019-10-20 21:27:31', '2019-10-20 21:27:31'),
(70, 1, 'admin/git-servers/create', 'GET', '127.0.0.1', '[]', '2019-10-20 21:31:38', '2019-10-20 21:31:38'),
(71, 1, 'admin/git-servers', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 21:31:45', '2019-10-20 21:31:45'),
(72, 1, 'admin/git-servers/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 21:31:58', '2019-10-20 21:31:58'),
(73, 1, 'admin/git-servers/create', 'GET', '127.0.0.1', '[]', '2019-10-20 21:37:36', '2019-10-20 21:37:36'),
(74, 1, 'admin/git-servers/create', 'GET', '127.0.0.1', '[]', '2019-10-20 21:37:52', '2019-10-20 21:37:52'),
(75, 1, 'admin/git-servers/create', 'GET', '127.0.0.1', '[]', '2019-10-20 21:43:27', '2019-10-20 21:43:27'),
(76, 1, 'admin/git-servers/create', 'GET', '127.0.0.1', '[]', '2019-10-20 21:55:42', '2019-10-20 21:55:42'),
(77, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 22:31:06', '2019-10-20 22:31:06'),
(78, 1, 'admin/auth/setting', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 22:31:30', '2019-10-20 22:31:30'),
(79, 1, 'admin/auth/setting', 'PUT', '127.0.0.1', '{\"name\":\"Administrator\",\"password\":\"$2y$10$A312zBSzT0czAhfadI3cfuEtQcsryITrPgHcSvMFKiPithyDYQtXm\",\"password_confirmation\":\"$2y$10$A312zBSzT0czAhfadI3cfuEtQcsryITrPgHcSvMFKiPithyDYQtXm\",\"_token\":\"a5DhdPsJfZa3UPvsyS7IFHWPYN2SmssJritP0yDo\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/localhost:8000\\/admin\\/auth\\/menu\"}', '2019-10-20 22:32:56', '2019-10-20 22:32:56'),
(80, 1, 'admin/auth/setting', 'GET', '127.0.0.1', '[]', '2019-10-20 22:32:57', '2019-10-20 22:32:57'),
(81, 1, 'admin/auth/setting', 'GET', '127.0.0.1', '[]', '2019-10-20 22:33:04', '2019-10-20 22:33:04'),
(82, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 22:33:10', '2019-10-20 22:33:10'),
(83, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 22:33:15', '2019-10-20 22:33:15'),
(84, 1, 'admin/auth/users/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 22:33:19', '2019-10-20 22:33:19'),
(85, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 22:50:39', '2019-10-20 22:50:39'),
(86, 1, 'admin/git-servers', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 22:53:08', '2019-10-20 22:53:08'),
(87, 1, 'admin/git-servers/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-20 22:53:35', '2019-10-20 22:53:35'),
(88, 1, 'admin/git-servers', 'POST', '127.0.0.1', '{\"url\":\"git\",\"user\":\"yasukosan\",\"passwd\":\"yasuko0704\",\"git_type_id\":\"1\",\"_token\":\"a5DhdPsJfZa3UPvsyS7IFHWPYN2SmssJritP0yDo\",\"_previous_\":\"http:\\/\\/localhost:8000\\/admin\\/git-servers\"}', '2019-10-20 23:04:49', '2019-10-20 23:04:49'),
(89, 1, 'admin/git-servers/create', 'GET', '127.0.0.1', '[]', '2019-10-20 23:04:52', '2019-10-20 23:04:52'),
(90, 1, 'admin/git-servers/create', 'GET', '127.0.0.1', '[]', '2019-10-20 23:05:25', '2019-10-20 23:05:25'),
(91, 1, 'admin/git-servers', 'POST', '127.0.0.1', '{\"url\":\"git\",\"user\":\"yasukosan\",\"passwd\":\"yasuko0704\",\"gittype_id\":\"1\",\"_token\":\"a5DhdPsJfZa3UPvsyS7IFHWPYN2SmssJritP0yDo\"}', '2019-10-20 23:05:37', '2019-10-20 23:05:37'),
(92, 1, 'admin/git-servers', 'GET', '127.0.0.1', '[]', '2019-10-20 23:05:38', '2019-10-20 23:05:38'),
(93, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '[]', '2019-10-22 22:10:57', '2019-10-22 22:10:57'),
(94, 1, 'admin', 'GET', '127.0.0.1', '[]', '2019-10-31 23:33:11', '2019-10-31 23:33:11'),
(95, 1, 'admin/git-servers', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-31 23:33:17', '2019-10-31 23:33:17'),
(96, 1, 'admin/git-servers/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-31 23:33:19', '2019-10-31 23:33:19'),
(97, 1, 'admin/git-servers/create', 'GET', '127.0.0.1', '[]', '2019-10-31 23:58:52', '2019-10-31 23:58:52'),
(98, 1, 'admin/git-servers', 'GET', '127.0.0.1', '[]', '2019-10-31 23:59:10', '2019-10-31 23:59:10'),
(99, 1, 'admin/git-types', 'GET', '127.0.0.1', '[]', '2019-10-31 23:59:54', '2019-10-31 23:59:54'),
(100, 1, 'admin/git-types/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-10-31 23:59:59', '2019-10-31 23:59:59'),
(101, 1, 'admin/git-types', 'POST', '127.0.0.1', '{\"uname\":\"GitHub\",\"url\":\"https:\\/\\/github.com\\/\",\"_token\":\"XyLe5Kn2YO0ZaKlaZk5gcjfldWvAa5OE3HXEoPdg\",\"_previous_\":\"http:\\/\\/localhost:8000\\/admin\\/git-types\"}', '2019-11-01 00:00:34', '2019-11-01 00:00:34'),
(102, 1, 'admin/git-types', 'GET', '127.0.0.1', '[]', '2019-11-01 00:00:35', '2019-11-01 00:00:35'),
(103, 1, 'admin/git-servers/create', 'GET', '127.0.0.1', '[]', '2019-11-01 00:00:46', '2019-11-01 00:00:46'),
(104, 1, 'admin/git-servers', 'POST', '127.0.0.1', '{\"url\":\"git\",\"user\":\"yasuko\",\"passwd\":\"yasuko0704\",\"token\":\"68619e327043974771e32ed79c42a467ad016972\",\"gittype_id\":\"2\",\"_token\":\"XyLe5Kn2YO0ZaKlaZk5gcjfldWvAa5OE3HXEoPdg\"}', '2019-11-01 00:00:59', '2019-11-01 00:00:59'),
(105, 1, 'admin/git-servers', 'GET', '127.0.0.1', '[]', '2019-11-01 00:00:59', '2019-11-01 00:00:59');

-- --------------------------------------------------------

--
-- テーブルの構造 `admin_permissions`
--

CREATE TABLE `admin_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `http_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `http_path` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- テーブルのデータのダンプ `admin_permissions`
--

INSERT INTO `admin_permissions` (`id`, `name`, `slug`, `http_method`, `http_path`, `created_at`, `updated_at`) VALUES
(1, 'All permission', '*', '', '*', NULL, NULL),
(2, 'Dashboard', 'dashboard', 'GET', '/', NULL, NULL),
(3, 'Login', 'auth.login', '', '/auth/login\r\n/auth/logout', NULL, NULL),
(4, 'User setting', 'auth.setting', 'GET,PUT', '/auth/setting', NULL, NULL),
(5, 'Auth management', 'auth.management', '', '/auth/roles\r\n/auth/permissions\r\n/auth/menu\r\n/auth/logs', NULL, NULL);

-- --------------------------------------------------------

--
-- テーブルの構造 `admin_roles`
--

CREATE TABLE `admin_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- テーブルのデータのダンプ `admin_roles`
--

INSERT INTO `admin_roles` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'administrator', '2019-10-17 22:40:56', '2019-10-17 22:40:56');

-- --------------------------------------------------------

--
-- テーブルの構造 `admin_role_menu`
--

CREATE TABLE `admin_role_menu` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- テーブルのデータのダンプ `admin_role_menu`
--

INSERT INTO `admin_role_menu` (`role_id`, `menu_id`, `created_at`, `updated_at`) VALUES
(1, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- テーブルの構造 `admin_role_permissions`
--

CREATE TABLE `admin_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- テーブルのデータのダンプ `admin_role_permissions`
--

INSERT INTO `admin_role_permissions` (`role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- テーブルの構造 `admin_role_users`
--

CREATE TABLE `admin_role_users` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- テーブルのデータのダンプ `admin_role_users`
--

INSERT INTO `admin_role_users` (`role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- テーブルの構造 `admin_users`
--

CREATE TABLE `admin_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- テーブルのデータのダンプ `admin_users`
--

INSERT INTO `admin_users` (`id`, `username`, `password`, `name`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$A312zBSzT0czAhfadI3cfuEtQcsryITrPgHcSvMFKiPithyDYQtXm', 'Administrator', 'images/001.jpg', 'RYJ4ufitCxPv9CZE3qcfnYjigIW3xnMhEzjUmYj9g0EoGc5wjEiXLCD7ahpC', '2019-10-17 22:40:56', '2019-10-20 22:32:57');

-- --------------------------------------------------------

--
-- テーブルの構造 `admin_user_permissions`
--

CREATE TABLE `admin_user_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- テーブルの構造 `docker_images`
--

CREATE TABLE `docker_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'イメージ名',
  `layer` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'ディスクレイヤーのhash値',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- テーブルの構造 `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- テーブルの構造 `git_branch`
--

CREATE TABLE `git_branch` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `repos_id` int(10) UNSIGNED NOT NULL,
  `commit_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'コミットのアドレス',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'コミット名',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `gitserver_id` int(10) UNSIGNED NOT NULL,
  `gittype_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- テーブルのデータのダンプ `git_branch`
--

INSERT INTO `git_branch` (`id`, `repos_id`, `commit_url`, `name`, `created_at`, `updated_at`, `gitserver_id`, `gittype_id`) VALUES
(112, 45, '', 'master', '2019-11-15 08:13:57', '2019-11-15 08:13:57', 1, 1),
(113, 5, '', 'dev', '2019-11-14 23:24:56', '2019-11-14 23:24:56', 1, 1),
(114, 5, '', 'dev2', '2019-11-14 23:24:56', '2019-11-14 23:24:56', 1, 1),
(115, 5, '', 'master', '2019-11-14 23:24:56', '2019-11-14 23:24:56', 1, 1),
(116, 8, '', 'master', '2019-11-14 23:24:58', '2019-11-14 23:24:58', 1, 1),
(117, 6, '', 'master', '2019-11-14 23:24:59', '2019-11-14 23:24:59', 1, 1),
(118, 7, '', 'master', '2019-11-14 23:25:00', '2019-11-14 23:25:00', 1, 1),
(119, 9, '', 'master', '2019-11-14 23:25:02', '2019-11-14 23:25:02', 1, 1),
(120, 10, '', 'master', '2019-11-14 23:25:03', '2019-11-14 23:25:03', 1, 1),
(121, 11, '', 'master', '2019-11-14 23:25:04', '2019-11-14 23:25:04', 1, 1),
(122, 12, '', 'master', '2019-11-14 23:25:05', '2019-11-14 23:25:05', 1, 1),
(123, 13, '', 'master', '2019-11-14 23:25:07', '2019-11-14 23:25:07', 1, 1),
(124, 14, '', 'master', '2019-11-14 23:25:08', '2019-11-14 23:25:08', 1, 1),
(125, 15, '', 'devel', '2019-11-14 23:25:09', '2019-11-14 23:25:09', 1, 1),
(126, 15, '', 'master', '2019-11-14 23:25:09', '2019-11-14 23:25:09', 1, 1),
(127, 16, '', 'develop', '2019-11-14 23:25:11', '2019-11-14 23:25:11', 1, 1),
(128, 16, '', 'master', '2019-11-14 23:25:11', '2019-11-14 23:25:11', 1, 1),
(129, 17, '', 'master', '2019-11-14 23:25:12', '2019-11-14 23:25:12', 1, 1),
(130, 18, '', 'master', '2019-11-14 23:25:13', '2019-11-14 23:25:13', 1, 1),
(131, 19, '', 'master', '2019-11-14 23:25:14', '2019-11-14 23:25:14', 1, 1),
(132, 20, '', 'master', '2019-11-14 23:25:16', '2019-11-14 23:25:16', 1, 1),
(133, 21, '', 'master', '2019-11-14 23:25:17', '2019-11-14 23:25:17', 1, 1),
(134, 22, '', 'master', '2019-11-14 23:25:19', '2019-11-14 23:25:19', 1, 1),
(135, 23, '', 'master', '2019-11-14 23:25:20', '2019-11-14 23:25:20', 1, 1),
(136, 24, '', 'master', '2019-11-14 23:25:22', '2019-11-14 23:25:22', 1, 1),
(137, 25, '', 'devel', '2019-11-14 23:25:23', '2019-11-14 23:25:23', 1, 1),
(138, 25, '', 'master', '2019-11-14 23:25:23', '2019-11-14 23:25:23', 1, 1),
(139, 26, '', 'master', '2019-11-14 23:25:25', '2019-11-14 23:25:25', 1, 1),
(140, 27, '', 'master', '2019-11-14 23:25:26', '2019-11-14 23:25:26', 1, 1),
(141, 28, '', 'master', '2019-11-14 23:25:27', '2019-11-14 23:25:27', 1, 1),
(142, 29, '', 'master', '2019-11-14 23:25:29', '2019-11-14 23:25:29', 1, 1),
(143, 30, '', 'dev_mo', '2019-11-14 23:25:30', '2019-11-14 23:25:30', 1, 1),
(144, 30, '', 'master', '2019-11-14 23:25:30', '2019-11-14 23:25:30', 1, 1),
(145, 31, '', 'dev', '2019-11-14 23:25:31', '2019-11-14 23:25:31', 1, 1),
(146, 31, '', 'master', '2019-11-14 23:25:31', '2019-11-14 23:25:31', 1, 1),
(147, 32, '', 'dev_mongo', '2019-11-14 23:25:33', '2019-11-14 23:25:33', 1, 1),
(148, 32, '', 'dev_multi_user', '2019-11-14 23:25:33', '2019-11-14 23:25:33', 1, 1),
(149, 32, '', 'dev_record', '2019-11-14 23:25:33', '2019-11-14 23:25:33', 1, 1),
(150, 32, '', 'dev_single_stream', '2019-11-14 23:25:33', '2019-11-14 23:25:33', 1, 1),
(151, 32, '', 'image_deploy', '2019-11-14 23:25:33', '2019-11-14 23:25:33', 1, 1),
(152, 32, '', 'master', '2019-11-14 23:25:33', '2019-11-14 23:25:33', 1, 1),
(153, 32, '', 'net_sample', '2019-11-14 23:25:33', '2019-11-14 23:25:33', 1, 1),
(154, 34, '', 'master', '2019-11-14 23:25:35', '2019-11-14 23:25:35', 1, 1),
(155, 34, '', 'php5.6-apache', '2019-11-14 23:25:35', '2019-11-14 23:25:35', 1, 1),
(156, 34, '', 'php7.0-apache', '2019-11-14 23:25:35', '2019-11-14 23:25:35', 1, 1),
(157, 34, '', 'php7.2-apache', '2019-11-14 23:25:35', '2019-11-14 23:25:35', 1, 1),
(158, 36, '', 'dev_screenedit', '2019-11-14 23:25:37', '2019-11-14 23:25:37', 1, 1),
(159, 36, '', 'dev_webrtc_version', '2019-11-14 23:25:37', '2019-11-14 23:25:37', 1, 1),
(160, 36, '', 'dev_websocket', '2019-11-14 23:25:37', '2019-11-14 23:25:37', 1, 1),
(161, 36, '', 'master', '2019-11-14 23:25:37', '2019-11-14 23:25:37', 1, 1),
(162, 38, '', 'master', '2019-11-14 23:25:40', '2019-11-14 23:25:40', 1, 1),
(163, 39, '', 'master', '2019-11-14 23:25:41', '2019-11-14 23:25:41', 1, 1),
(164, 41, '', 'master', '2019-11-14 23:25:44', '2019-11-14 23:25:44', 1, 1),
(165, 42, '', 'master', '2019-11-14 23:25:45', '2019-11-14 23:25:45', 1, 1),
(166, 43, '', 'master', '2019-11-14 23:25:46', '2019-11-14 23:25:46', 1, 1);

-- --------------------------------------------------------

--
-- テーブルの構造 `git_commit`
--

CREATE TABLE `git_commit` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `gitserver_id` int(10) UNSIGNED NOT NULL,
  `gittype_id` int(10) UNSIGNED NOT NULL,
  `repos_id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `hash` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'コミット識別用のハッシュ値',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'コミット名',
  `date` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'コミット日',
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'コミットメッセージ',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- テーブルの構造 `git_repos`
--

CREATE TABLE `git_repos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `gitserver_id` int(10) UNSIGNED NOT NULL,
  `branch_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'ブランチのアドレス',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'ブランチ名',
  `mainbranch` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'メインのブランチ名（だいたいmaster）',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- テーブルのデータのダンプ `git_repos`
--

INSERT INTO `git_repos` (`id`, `gitserver_id`, `branch_url`, `name`, `mainbranch`, `created_at`, `updated_at`) VALUES
(1, 2, '', 'print-service', 'master', '2019-11-13 01:14:42', '2019-11-13 01:14:42'),
(2, 2, '', 'SC', 'master', '2019-11-13 01:14:42', '2019-11-13 01:14:42'),
(3, 2, '', 'shipyard', 'master', '2019-11-13 01:14:42', '2019-11-13 01:14:42'),
(4, 2, '', 'vmeeting', 'master', '2019-11-13 01:14:42', '2019-11-13 01:14:42'),
(5, 1, '', 'Reserve', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(6, 1, '', 'reserve_ver1', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(7, 1, '', 'CakeTest', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(8, 1, '', 'Pazzdra', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(9, 1, '', 'SendView', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(10, 1, '', 'Tokumori_Webpage', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(11, 1, '', 'wether_report', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(12, 1, '', 'Finance', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(13, 1, '', 'pmap', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(14, 1, '', 'opencv_test', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(15, 1, '', 'Webrtc_test', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(16, 1, '', 'SeleniumTest', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(17, 1, '', 'ReceptionAndSender', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(18, 1, '', 'Angular4Test', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(19, 1, '', 'pazz-manager', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(20, 1, '', 'FinanceTracker', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(21, 1, '', 'print-service', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(22, 1, '', 'ReserveTest', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(23, 1, '', 'Kasekora', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(24, 1, '', 'Guardian', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(25, 1, '', 'directive-test', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(26, 1, '', 'guardian_server', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(27, 1, '', 'Catalog', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(28, 1, '', 'API_TESTER', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(29, 1, '', 'DnsAPI', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(30, 1, '', 'AutoDeploy', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(31, 1, '', 'mo_php', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(32, 1, '', 'Paint-Chat', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(33, 1, '', 'MobileTest', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(34, 1, '', 'docker_image', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(35, 1, '', 'onsen', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(36, 1, '', 'janus_ts', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(37, 1, '', 'janus_exercise', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(38, 1, '', 'vMeeting', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(39, 1, '', 'vochan', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(40, 1, '', 'token_server', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(41, 1, '', 'SC', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(42, 1, '', 'UzaSubmodule', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(43, 1, '', 'AglibModule', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(44, 1, '', 'laravelAdminTest', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17'),
(45, 1, '', 'RestHub', 'master', '2019-11-12 17:09:17', '2019-11-12 17:09:17');

-- --------------------------------------------------------

--
-- テーブルの構造 `git_server`
--

CREATE TABLE `git_server` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `url` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'GitレジストラURL',
  `user` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'ログインID',
  `passwd` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'パスワード',
  `gittype_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'アクセストークン'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- テーブルのデータのダンプ `git_server`
--

INSERT INTO `git_server` (`id`, `url`, `user`, `passwd`, `gittype_id`, `created_at`, `updated_at`, `token`) VALUES
(1, 'git', 'yasukosan', 'yasuko0704', 1, '2019-10-20 23:05:37', '2019-10-20 23:05:37', ''),
(2, 'git', 'yasuko', 'yasuko0704', 2, '2019-11-01 00:00:59', '2019-11-06 02:05:25', 'b7b504221ad6bb98f2785eacffe39f1f2bdffc69');

-- --------------------------------------------------------

--
-- テーブルの構造 `git_type`
--

CREATE TABLE `git_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uname` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'リポジトリ名',
  `url` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'リポジトリのURL',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- テーブルのデータのダンプ `git_type`
--

INSERT INTO `git_type` (`id`, `uname`, `url`, `created_at`, `updated_at`) VALUES
(1, 'BitBacket', 'https://bitbucket.org', '2019-10-21 02:44:42', '2019-10-24 01:17:19'),
(2, 'GitHub', 'https://github.com/', '2019-11-01 09:00:34', '2019-11-01 09:00:34');

-- --------------------------------------------------------

--
-- テーブルの構造 `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `node_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `word1` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `word2` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `word3` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `job` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `job_group` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- テーブルの構造 `job_stack`
--

CREATE TABLE `job_stack` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `gittype_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- テーブルの構造 `line_user`
--

CREATE TABLE `line_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- テーブルの構造 `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- テーブルのデータのダンプ `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2019_10_18_040236_docker_images', 2),
(10, '2019_10_18_042038_git', 2),
(11, '2019_10_18_042158_jobs', 2),
(12, '2019_10_18_042321_line_user', 2),
(13, '2019_10_18_042340_response_stack', 2),
(14, '2016_01_04_173148_create_admin_tables', 3),
(15, '2019_11_01_073325_git_server_table_update', 4),
(16, '2019_11_13_074331_update_gitcommit', 5),
(17, '2019_11_13_074352_update_gitbranch', 5);

-- --------------------------------------------------------

--
-- テーブルの構造 `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- テーブルの構造 `response_stack`
--

CREATE TABLE `response_stack` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- テーブルの構造 `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `admin_menu`
--
ALTER TABLE `admin_menu`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `admin_operation_log`
--
ALTER TABLE `admin_operation_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_operation_log_user_id_index` (`user_id`);

--
-- テーブルのインデックス `admin_permissions`
--
ALTER TABLE `admin_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_permissions_name_unique` (`name`),
  ADD UNIQUE KEY `admin_permissions_slug_unique` (`slug`);

--
-- テーブルのインデックス `admin_roles`
--
ALTER TABLE `admin_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_roles_name_unique` (`name`),
  ADD UNIQUE KEY `admin_roles_slug_unique` (`slug`);

--
-- テーブルのインデックス `admin_role_menu`
--
ALTER TABLE `admin_role_menu`
  ADD KEY `admin_role_menu_role_id_menu_id_index` (`role_id`,`menu_id`);

--
-- テーブルのインデックス `admin_role_permissions`
--
ALTER TABLE `admin_role_permissions`
  ADD KEY `admin_role_permissions_role_id_permission_id_index` (`role_id`,`permission_id`);

--
-- テーブルのインデックス `admin_role_users`
--
ALTER TABLE `admin_role_users`
  ADD KEY `admin_role_users_role_id_user_id_index` (`role_id`,`user_id`);

--
-- テーブルのインデックス `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_users_username_unique` (`username`);

--
-- テーブルのインデックス `admin_user_permissions`
--
ALTER TABLE `admin_user_permissions`
  ADD KEY `admin_user_permissions_user_id_permission_id_index` (`user_id`,`permission_id`);

--
-- テーブルのインデックス `docker_images`
--
ALTER TABLE `docker_images`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `git_branch`
--
ALTER TABLE `git_branch`
  ADD PRIMARY KEY (`id`),
  ADD KEY `git_branch_repos_id_index` (`repos_id`);

--
-- テーブルのインデックス `git_commit`
--
ALTER TABLE `git_commit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `git_commit_repos_id_branch_id_index` (`repos_id`,`branch_id`);

--
-- テーブルのインデックス `git_repos`
--
ALTER TABLE `git_repos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `git_repos_gitserver_id_index` (`gitserver_id`);

--
-- テーブルのインデックス `git_server`
--
ALTER TABLE `git_server`
  ADD PRIMARY KEY (`id`),
  ADD KEY `git_server_gittype_id_index` (`gittype_id`);

--
-- テーブルのインデックス `git_type`
--
ALTER TABLE `git_type`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_node_id_job_group_index` (`node_id`,`job_group`);

--
-- テーブルのインデックス `job_stack`
--
ALTER TABLE `job_stack`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_stack_gittype_id_index` (`gittype_id`);

--
-- テーブルのインデックス `line_user`
--
ALTER TABLE `line_user`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- テーブルのインデックス `response_stack`
--
ALTER TABLE `response_stack`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- ダンプしたテーブルのAUTO_INCREMENT
--

--
-- テーブルのAUTO_INCREMENT `admin_menu`
--
ALTER TABLE `admin_menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- テーブルのAUTO_INCREMENT `admin_operation_log`
--
ALTER TABLE `admin_operation_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- テーブルのAUTO_INCREMENT `admin_permissions`
--
ALTER TABLE `admin_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- テーブルのAUTO_INCREMENT `admin_roles`
--
ALTER TABLE `admin_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- テーブルのAUTO_INCREMENT `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- テーブルのAUTO_INCREMENT `docker_images`
--
ALTER TABLE `docker_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- テーブルのAUTO_INCREMENT `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- テーブルのAUTO_INCREMENT `git_branch`
--
ALTER TABLE `git_branch`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=167;

--
-- テーブルのAUTO_INCREMENT `git_commit`
--
ALTER TABLE `git_commit`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- テーブルのAUTO_INCREMENT `git_repos`
--
ALTER TABLE `git_repos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- テーブルのAUTO_INCREMENT `git_server`
--
ALTER TABLE `git_server`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- テーブルのAUTO_INCREMENT `git_type`
--
ALTER TABLE `git_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- テーブルのAUTO_INCREMENT `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- テーブルのAUTO_INCREMENT `job_stack`
--
ALTER TABLE `job_stack`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- テーブルのAUTO_INCREMENT `line_user`
--
ALTER TABLE `line_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- テーブルのAUTO_INCREMENT `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- テーブルのAUTO_INCREMENT `response_stack`
--
ALTER TABLE `response_stack`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- テーブルのAUTO_INCREMENT `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
