<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('getcsrf', 'MainController@getCSRFToken');

Route::prefix('git')->group(function() {
    Route::get('type', 'GitController@get_type');
    Route::get('account', 'GitController@get_account');
    Route::get('repository/{type}/{id}', 'GitController@get_repository');
    Route::get('branch/{type}/{id}/{repos}', 'GitController@get_branch');
    Route::get('commit/{type}/{id}/{repos}/{branch}', 'GitController@get_commit');
    
    Route::get('update_branch/{type}/{id}/{repos}', 'GitController@update_branch');
    
    Route::put('repository/{type}/{id}', 'GitController@index');
    Route::put('branch/{type}/{id}/{repos}', 'GitController@index');
    Route::put('commit/{type}/{id}/{repos}/{branch}', 'GitController@index');
});

Route::prefix('docker')->group(function () {
    Route::get('containers', 'DockerController@getActiveContainers');
    // Route::get('containers/{}', 'DockerController@getActiveContainers');
    Route::post('setcontainers', 'DockerController@buildContainer');
    Route::delete('delcontainers', 'DockerController@deleteContainer');
});

Route::prefix('proxmox')->group(function () {
    Route::get('lxclist', 'ProxmoxController@get_lxc_list');
    Route::post('addlxc', 'ProxmoxController@add_lxc');
    Route::post('dellxc', 'ProxmoxController@del_lxc');
});

Route::get('redis', 'DockerController@redisTest');

Route::post('git', 'GitController@index');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
