
        local redis = require "resty.redis"
        local red = redis:new()
        local args = ngx.req.get_uri_args()

        local ok, err = red:connect("192.168.2.175", 6379)

        if not ok then
            ngx.say("failed to connect: ", err)
            return
        end

        domain_table = {}
        for i in string.gmatch(ngx.var.host, "([^.]+)") do
            table.insert(domain_table, i)
        end

        if table.getn(domain_table) == 3 then
            ngx.var.proxy_to, err = red:get(domain_table[1])
        end

        if not ngx.var.proxy_to then
            ngx.say("failed to get token: ", ngx.var.proxy_to)
            return
        end

        red:close()




