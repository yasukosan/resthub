<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Jobs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('node_id')->default(0);
            $table->string('word1', 20)->default('');
            $table->string('word2', 20)->default('');
            $table->string('word3', 20)->default('');
            $table->string('job', 50)->default('');
            $table->unsignedInteger('job_group')->default(0);
            $table->string('description', 100)->default('');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->index(['node_id', 'job_group']);
        });
        Schema::create('job_stack', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id', 100);
            $table->string('job', 50);
            $table->text('message');
            $table->unsignedInteger('gittype_id');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->index('gittype_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
        Schema::dropIfExists('job_stack');
    }
}
