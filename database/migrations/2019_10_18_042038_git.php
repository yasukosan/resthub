<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Git extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('git_repos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('gitserver_id');
            $table->string('branch_url', 100)->comment('ブランチのアドレス');
            $table->string('name', 50)->comment('ブランチ名');
            $table->string('mainbranch', 100)->comment('メインのブランチ名（だいたいmaster）');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->index('gitserver_id');
        });
        Schema::create('git_branch', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('repos_id');
            $table->string('commit_url', 100)->comment('コミットのアドレス');
            $table->string('name', 50)->comment('コミット名');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->index('repos_id');
        });
        Schema::create('git_commit', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('repos_id');
            $table->unsignedInteger('branch_id');
            $table->string('hash', 50)->comment('コミット識別用のハッシュ値');
            $table->string('name', 50)->comment('コミット名');
            $table->string('date', 50)->comment('コミット日');
            $table->text('message')->comment('コミットメッセージ');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->index(['repos_id', 'branch_id']);
        });
        Schema::create('git_server', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('url', 50)->comment('GitレジストラURL');
            $table->string('user', 50)->comment('ログインID');
            $table->string('passwd', 50)->comment('パスワード');
            $table->unsignedInteger('gittype_id')->default(0);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->index('gittype_id');
        });
        Schema::create('git_type', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uname', 10)->comment('リポジトリ名');
            $table->string('url', 150)->comment('リポジトリのURL');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('git_repos');
        Schema::dropIfExists('git_branch');
        Schema::dropIfExists('git_commit');
        Schema::dropIfExists('git_server');
        Schema::dropIfExists('git_type');
    }
}
