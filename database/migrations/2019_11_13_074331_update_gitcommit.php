<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateGitcommit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('git_commit', function (Blueprint $table) {
            $table->unsignedInteger('gitserver_id');
            $table->unsignedInteger('gittype_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('git_commit', function (Blueprint $table) {
            $table->dropColumn('gitserver_id');
            $table->dropColumn('gittype_id');
        });
    }
}
