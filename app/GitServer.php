<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GitServer extends Model
{
    protected $table = 'git_server';
    protected $fillable = [
        'url',
        'user',
        'passwd',
        'gittype_id',
    ];
    public function git_type()
    {
        return $this->hasOne(GitType::class, 'id', 'gittype_id');
    }
}

