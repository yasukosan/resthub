<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GitRepos extends Model
{
    protected $table = 'git_repos';
    protected $fillable = array('name', 'gitserver_id', 'mainbranch', 'branch_url');


    /**
     * リポジトリ情報の更新
     * 登録がない場合：　全て登録
     * 登録がある場合：　重複していないリポジトリを登録
     *
     * @param Array $repos
     * @return bool
     */
    public static function updateRepositorys(Array $repos): bool
    {
        // リポジトリ一覧取得
        $_repos = self::all();

        // リポジトリが空の場合全件登録
        if ($_repos->isEmpty()) {
            foreach ($repos as $repo) {
                self::insert($repo);
            }
        } else {

            // 新規取得したリポジトリに存在していないが
            // 既存のテーブルに存在しているリポジトリを削除する
            /*
            foreach ($_repos as $value) {
                if (!array_key_exists($value->name, $repos) ) {
                    self::where('id', $value->id)->delete();
                }
            }*/

            // dd($repos);
            $flag = 0;
            // 既にあるリポジトリは無視、無い場合は作成
            foreach ($repos as $repo) {
                self::firstOrCreate(
                    [
                        'name'          => $repo['name'],
                        'gitserver_id'  => $repo['gitserver_id'],
                    ],
                    [
                        'branch_url'    => $repo['branch_url'],
                        'mainbranch'    => $repo['mainbranch'],
                    ]
                );
                if ($commit->wasRecentlyCreated) {
                    $flag = true;
                }
            }
            return $flag;
        }
    }

}
