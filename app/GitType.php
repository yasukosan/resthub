<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GitType extends Model
{
    protected $table = 'git_type';
    public $timestamps = false;

    protected $fillable = [
        'uname',
        'url',
    ];
    public function git_server()
    {
        return $this->belongsTo(Exampleusers::class, 'git_server_id');
    }
}
