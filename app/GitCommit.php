<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GitCommit extends Model
{
    protected $table = 'git_commit';
    protected $fillable = array(
        'gitserver_id', 'gittype_id', 'repos_id',
        'branch_id', 'hash', 'name', 'date', 'message');

    /**
     * コミット情報を更新
     * 登録コミットが０件：　全て登録
     * 既に登録がある　　：　登録済みの内容意外を追加
     *
     * @param Array $commits
     * @return boolean
     */
    public static function updateCommits(Array $commits): bool
    {
        // リポジトリ一覧取得
        $_commits = self::all();

        // リポジトリが空の場合全件登録
        if ($_commits->isEmpty()) {
            foreach ($commits as $commit) {
                self::insert($commit);
            }
            $flag = true;
        } else {

            /*
            // 新規取得したリポジトリに存在していないが
            // 既存のテーブルに存在しているリポジトリを削除する
            foreach ($_commits as $value) {
                $search = true;
                foreach ($commits as $commit) {
                    if ($value->repos_id        == $commit['repos_id']
                        && $value->branch_id    == $commit['branch_id']
                        && $value->name         == $commit['name'])
                    {
                        $search = false;
                    }
                }
                if ($search) {
                    self::where('id', $value->id)->delete();
                }
            }*/

            // 既にあるリポジトリは無視、無い場合は作成
            $flag = false;
            foreach ($commits as $commit) {
                $commit = self::firstOrCreate(
                    [
                        'gitserver_id'  => $branch['gitserver_id'],
                        'gittype_id'    => $branch['gittype_id'],
                        'repos_id'      => $commit['repos_id'],
                        'branch_id'     => $commit['branch_id'],
                        'name'          => $commit['name'],
                    ],
                    [
                        'hash'          => $commit['hash'],
                        'date'          => $commit['date'],
                        'message'       => $commit['message'],
                    ]
                );
                if ($commit->wasRecentlyCreated) {
                    $flag = true;
                }
            }

            return $flag;
        }
    }

    /**
     * リポジトリ、ブランチに紐付けされたコミットを
     * 全て削除（リセット）
     *
     * @param integer $gitserver_id
     * @param integer $gittype_id
     * @param integer $repos_id
     * @param integer $branch_id
     * @return void
     */
    public static function resetCommit(
        int $gitserver_id, int $gittype_id, int $repos_id, int $branch_id
    ): void {
        self::where('gitserver_id', $gitserver_id)
            ->where('gittype_id', $gittype_id)
            ->delete();
    }
}
