<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;

class RedisServer
{
    // Redisインスタンスの格納
    private $redis = null;
    
    public function __construct()
    {
        $this->setup();
    }

    /**
     * Redisから値を取得
     *
     * @param string $key 取得キー名
     * @return string 取得文字列
     */
    public function getKey(string $key): string
    {
        return $this->redis->get($key);
    }

    /**
     * Redisに値を登録する
     *
     * @param string $key 登録キー名
     * @param string $param 登録する値
     * @return void
     */
    public function setKey(string $key, string $param): void
    {
        $this->redis->set($key, $param);
    }

    /**
     * Redisに値を登録し
     * URLを返す
     *
     * @param string $key
     * @param string $param
     * @return string
     */
    public function setKeyToURL(string $key, string $param): string
    {
        $url = $key . '.' . $this->base_url;
        $this->redis->set($key, $param);
        return $url;
    }
    /**
     * Redisからキーを削除する
     * （データも消える）
     *
     * @param string $key
     * @return void
     */
    public function delKey(string $key): void
    {
        $this->redis->delete([$key]);
    }

    /**
     * Redisインスタンスの作成
     * サーバーへの接続を行う
     *
     * @return void
     * インスタンス本体は「redis」変数に格納
     */
    private function setup(): void
    {
        if (!$this->redis) {
            $this->redis = Redis::connection();
        }
    }
}
