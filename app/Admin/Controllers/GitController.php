<?php

namespace App\Admin\Controllers;

use App\GitServer;
use App\GitType;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class GitController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\GitServer';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(GitServer::class, function (Grid $grid) {
            $grid->id('ID');
            $grid->url('リポジトリ');
            $grid->user('ログインID');
            $grid->passwd('パスワード');
            $grid->token('トークン');
            $grid->gittype_id('種別');
            // $tmp = GitType::where('id', )
        });
        //return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(GitServer::findOrFail($id));



        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(GitServer::class, function (Form $form) {
            // リポジトリ
            $form->display('id', 'ID');
            $form->text('url', 'リポジトリ')->rules('required');

            // ログインユーザー
            $form->text('user', 'ユーザー名')->rules('required|max:16');

            // パスワード
            $form->text('passwd', 'パスワード')->rules('required|max:16');

            // パスワード
            $form->text('token', 'トークン')->rules('required|max:100');

            // レジストラ
            $git_type_id_options = [];
            $tmp = GitType::orderBy('id')->get(['id', 'uname']);
            if ($tmp->isNotEmpty()) {
                $list = $tmp->all();
                foreach ($list as $v) {
                    $git_type_id_options[$v->id] = $v->uname;
                }
            }
            $form->select('gittype_id', 'レジストラ選択')
                ->options($git_type_id_options)->rules('required');
        });
    }
}
