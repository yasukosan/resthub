<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    public function getCSRFToken():string {
        return csrf_token();
    }
}
