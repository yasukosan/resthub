<?php

namespace App\Http\Controllers;

use App\GitType;
use App\GitServer;
use App\GitRepos;
use App\GitBranch;
use App\GitCommit;

use App\_lib\GitLib\Git;
use Illuminate\Http\Request;


class GitController extends Controller
{

    /**
     * GitType一覧を返す
     *
     * @return Object
     */
    public function get_type(): Object
    {
        return GitType::all();
    }

    /**
     * アカウント情報一覧を返す
     *
     * @return \Illuminate\Http\Response
     */
    public function get_account(): Object
    {
        $hoge = 0;
        return GitServer::all();
    }

    /**
     * アカウント情報から
     * 保存されたリポジトリ情報を返す
     *
     * @param Request $request
     * @param int $id
     * @param int $type
     * @return Object|null
     */
    public function get_repository(Request $request, int $type): ?Object
    {
        // リポジトリデータを全件返す
        return GitRepos::where('gitserver_id', $type)->get();
    }

    /**
     * リポジトリサーバーから最新のリポジトリを取得
     *
     * @param Request $request
     * @param integer $id
     * @param integer $type
     * @return Object|null
     */
    public function update_repository(Request $request, int $id, int $type): ?Object
    {
        // Gitタイプから該当のライブラリ取得
        $G = $this->iniGitLib($type);
        // アカウント情報設定
        $G->setGit(GitServer::where('id', $id)->first(), $type);

        // リポジトリデータ取得
        if ($G->getAllUserRepositorys()) {
            // リポジトリデータをテーブルに保存
            if (GitRepos::updateRepositorys($G->getRepositorys('all')))
            {
                // リポジトリが更新されたのでブランチとコミットをリセット
                GitBranch::resetBranch($type, $id);
                GitCommit::resetCommit($type, $id);
            };

            // リポジトリデータを全件返す
            return GitRepos::where('gitserver_id', $type)->get();
        }
        return null;
    }

    /**
     * アカウント情報、リポジトリ情報から
     * ブランチ情報を返す
     *
     * @param Request $request
     * @param int $type
     * @param int $id
     * @param int $repos
     * @return Object
     */
    public function get_branch(
        Request $request, int $type, int $id = 0, int $repos = 0
    ): ?Object {
        $branches = GitBranch::where('gitserver_id', $id)
                ->where('gittype_id', $type)
                ->where('repos_id', $repos)
                ->get();
        if ($branches->isEmpty()) {
            // return $this->update_branch($request, $type, $id, $repos);
        }
        return $branches;
    }

    /**
     * Gitリポジトリからブランチ情報を取得
     *
     * @param Request $request
     * @param integer $type
     * @param integer $id
     * @param integer $repos
     * @return Object|null
     */
    public function update_branch(
        Request $request, int $type, int $id = 0, int $repos = 0
    ): ?Object
    {
        // Gitタイプから該当のライブラリ取得
        $G = $this->iniGitLib($type);
        // Gitアカウント情報リポジトリ情報登録
        $G->setGit(GitServer::where('id', $id)->first(), $type)
            ->setRepository(GitRepos::where('id', $repos)->first());

        // ブランチデータ取得
        if ($G->getAllUserBranches()) {
            // ブランチデータをテーブルに保存
            GitBranch::updateBranches($G->getBranches('all'));
            // ブランチデータを全件返す
            return GitBranch::where('gitserver_id', $id)
                    ->where('gittype_id', $type)
                    ->where('repos_id', $repos)
                    ->get();
        }
    }

    /**
     * アカウント情報、リポジトリ情報、ブランチ情報から
     * コミット情報を返す
     *
     * @param Request $request
     * @param int $type
     * @param int $id
     * @param string $repos
     * @param string $branch
     * @return Object
     */
    public function get_commit(
        Request $request, int $type, int $id = 0, string $repos = '', string $branch = ''
    ): Object {
        // コミットデータを返す
        $commits = GitCommit::where('gitserver_id',$id)
                    ->where('gittype_id', $type)
                    ->where('repos_id', $repos)
                    ->where('branch_id', $branch)
                    ->get();
        if ($commits->isEmpty()) {
            return $this->update_commit($request, $type, $id, $repos, $branch);
        }
        return $commits;
        
    }

    public function update_commit(
        Request $request, int $type, int $id = 0, string $repos = '', string $branch = ''
    ): Object {
        // Gitタイプから該当のライブラリ取得
        $G = $this->iniGitLib($type);
        $G->setGit(GitServer::where('id', $id)->first(), $type)
            ->setRepository(GitRepos::where('id', $repos)->first())
            ->setBranch(GitBranch::where('id', $branch)->first());
        // BitBacketからコミットデータ取得
        if ($G->getAllUserCommits()) {
            // コミットデータをテーブルに保存
            GitCommit::updateCommits($G->getCommits());
            // コミットデータを返す
            return GitCommit::where('gitserver_id',$id)
                        ->where('gittype_id', $type)
                        ->where('repos_id', $repos)
                        ->where('branch_id', $branch)
                        ->get();
        }
    }


    public function get_gitserver(Request $request)
    {
        $tmp = GitBranch::where('gitserver_id', $request->gitserver_id);
        if ($tmp->isEmpty()) {

        }
    }


    /**
     * GitTypeから該当するGitライブラリを返す
     *
     * @param integer $type
     * @return Object
     */
    public function iniGitLib(int $type): Object
    {
        $tmp = GitType::where('id', $type)->first();
        $Lib = (string)$tmp->uname . 'Repository';
        return Git::$Lib();
    }

}
