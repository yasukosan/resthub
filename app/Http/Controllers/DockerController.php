<?php

namespace App\Http\Controllers;

use App\_lib\Docker\Docker;
use Illuminate\Http\Request;

use App\RedisServer;

class DockerController extends Controller
{
    /**
     * 起動中のコンテナ一覧を取得
     *
     * @return array
     */
    public function getActiveContainers(): array
    {
        return  Docker::DockerRepository()->getAllContainer();
    }


    /**
     * Dockerコンテナを新規作成
     * 
     * @param $param array
     * @return string
     * コンテナ作成成功の場合、コンテナへのアクセスURL
     * コンテナが作成済みの場合、作成済みのメッセージが返る
     */
    public function buildContainer(Request $request): string {
        $result = Docker::DockerRepository()->setNewContainer($request['data']);
        $redis = new RedisServer();
        $redis->setKey($request['data']['commit'], $result);
        return $result;
    }

    /**
     * Dockerコンテナを削除
     *
     * @param Request $request
     * @return void
     */
    public function deleteContainer(Request $request): void {
        $req = json_decode($request['data'], TRUE);
        Docker::DockerRepository()
                ->setDeleteContainer($req['name']);
        $redis = new RedisServer();
        $redis->delKey($req['name']);
    }

    public function redisTest(): string {
        $redis = new RedisServer();
        $redis->setKey('hoge', 'POWER YOUR UNKO');
        return $redis->getKey('hoge');
    }
}
