<?php

namespace App\Http\Controllers;

use App\_lib\Proxmox\Proxmox;
use Illuminate\Http\Request;

class ProxmoxController extends Controller
{
    private $px = null;

    public function __construct()
    {
        if (!$this->px) {
            $this->px = Proxmox::ProxmoxRepository();
        }
    }

    /**
     * 登録・稼働中のLXCコンテナのリストを取得
     *
     * @return array
     */
    public function get_lxc_list(): array
    {
        // トークンの確認、無ければ新規取得
        return $this->px->checkToken()
        // LXCコンテナの一覧をAPIから取得
                    ->getAllLXCFromAPI()
        // LXCリストを受け取る
                    ->getLXCList();
    }

    /**
     * LXCコンテナを新規作成
     *
     * @return array
     */
    public function add_lxc(): array
    {
        $this->px->checkToken()
                ->getAllLXCFromAPI()
                ->addLXCToAPI();
        return array('status'   => 'success');
    }

    /**
     * 稼働中・停止中のLXCコンテナを削除
     *
     * @return void
     */
    public function del_lxc(): array
    {
        $this->px->checkToken()
                ->delLXCToAPI();
        return array('status'   => 'success');
    }

}
