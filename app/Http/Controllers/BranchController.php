<?php

namespace App\Http\Controllers;

use App\GitBranch;
use Illuminate\Http\Request;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GitBranch  $gitBranch
     * @return \Illuminate\Http\Response
     */
    public function show(GitBranch $gitBranch)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GitBranch  $gitBranch
     * @return \Illuminate\Http\Response
     */
    public function edit(GitBranch $gitBranch)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GitBranch  $gitBranch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GitBranch $gitBranch)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GitBranch  $gitBranch
     * @return \Illuminate\Http\Response
     */
    public function destroy(GitBranch $gitBranch)
    {
        //
    }
}
