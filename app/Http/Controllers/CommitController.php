<?php

namespace App\Http\Controllers;

use App\GitCommit;
use Illuminate\Http\Request;

class CommitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GitCommit  $gitCommit
     * @return \Illuminate\Http\Response
     */
    public function show(GitCommit $gitCommit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GitCommit  $gitCommit
     * @return \Illuminate\Http\Response
     */
    public function edit(GitCommit $gitCommit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GitCommit  $gitCommit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GitCommit $gitCommit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GitCommit  $gitCommit
     * @return \Illuminate\Http\Response
     */
    public function destroy(GitCommit $gitCommit)
    {
        //
    }
}
