<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GitBranch extends Model
{
    protected $table = 'git_branch';
    protected $fillable = array(
        'gitserver_id', 'gittype_id', 'repos_id',
        'commit_url', 'name');

    /**
     * ブランチ情報を更新
     * 登録がない場合：　全てのブランチを登録
     * 登録がある場合：　重複していないブランチを登録
     *
     * @param Array $branches
     * @return boolean
     */
    public static function updateBranches(Array $branches): bool
    {
        // リポジトリ一覧取得
        $_branches = self::all();

        // リポジトリが空の場合全件登録
        if ($_branches->isEmpty()) {
            foreach ($branches as $branch) {
                self::insert($branch);
            }
        } else {

            /*
            // 新規取得したリポジトリに存在していないが
            // 既存のテーブルに存在しているリポジトリを削除する
            foreach ($_branches as $value) {
                if (!array_key_exists($value->name, $branches)) {
                    self::where('id', $value->id)->delete();
                }
            }
            */

            $flag = false;
            // 既にあるリポジトリは無視、無い場合は作成
            foreach ($branches as $branch) {
                $branch = self::firstOrCreate(
                    [
                        'gitserver_id'  => $branch['gitserver_id'],
                        'gittype_id'    => $branch['gittype_id'],
                        'repos_id'      => $branch['repos_id'],
                        'name'          => $branch['name'],
                    ],
                    [
                        'commit_url'    => $branch['commit_url'],
                    ]
                );
                if ($branch->wasRecentlyCreated) {
                    $flag = true;
                }
            }
            return $flag;
        }
    }

    /**
     * リポジトリに紐付けされた
     * ブランチの削除（リセット）
     *
     * @param integer $gitserver_id
     * @param integer $gittype_id
     * @param integer $repos_id
     * @return void
     */
    public static function resetBranch(
        int $gitserver_id, int $gittype_id, int $repos_id
    ): void {
        self::where('gitserver_id', $gitserver_id)
            ->where('gittype_id', $gittype_id)
            ->delete();
    }
}
